import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget bottomNavigationBar;
  final Widget child;
  final bool Function() willPopScope;
  final FloatingActionButton floatingActionButton;

  MainLayout({
    this.appBar,
    this.bottomNavigationBar,
    @required this.child,
    this.willPopScope,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (willPopScope != null) {
          return willPopScope();
        }
        return false;
      },
      child: Scaffold(
        appBar: appBar,
        body: child,
        bottomNavigationBar: bottomNavigationBar,
        floatingActionButton: floatingActionButton,
      ),
    );
  }
}
