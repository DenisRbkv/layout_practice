import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_practice/res/const.dart';

import 'package:layout_practice/layouts/main_layout.dart';
import 'package:layout_practice/utils/custom_app_bar.dart';

import 'package:layout_practice/pages/page_two.dart';


class PageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appBar: CustomAppBar(
        title: PAGE_ONE,
        context: context,
      ).returnAppBar(),
      child: Center(
        child: Text(
          PAGE_ONE,
          style: TextStyle(
            fontSize: ScreenUtil().setSp(18.0),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PageTwo(),
          ),
        ),
        child: FittedBox(
          child: Text(
            FLOAT_BUTTON,
            style: TextStyle(
              color: Colors.white,
              fontSize: 14.0,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
