import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:layout_practice/res/const.dart';

class CustomAppBar {
  final String title;
  final BuildContext context;

  CustomAppBar({@required this.title, @required this.context});

  PreferredSize returnAppBar() {
    return PreferredSize(
      preferredSize: _checkPage(),
      child: Container(
        height: double.infinity,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Text(
            title + APPBAR,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(20.0),
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  Size _checkPage() {
    if (this.title == PAGE_ONE) {
      return Size.fromHeight(75.0);
    } else {
      return Size.fromHeight(175.0);
    }
  }
}
